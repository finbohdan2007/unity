using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DialogueTrigger : MonoBehaviour
{
    [SerializeField] string FileName;
    [SerializeField] Animator player;
    Dialogue DialogueCreator()
    {
        try
        {
            Dialogue dialogue = new Dialogue();
            dialogue.sentences = new List<string>();
            TextAsset text = (TextAsset)Resources.Load(FileName);
            foreach (string sentence in text.text.Split("\n"))
            {
                dialogue.sentences.Add(sentence);
            }
            return dialogue;
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
            Dialogue dialogue = new Dialogue();
            dialogue.sentences = new List<string>();
            return dialogue;
        }
    }
    public void TriggerDialogue()
    {
        player.SetBool("isTalking", true);
        FindObjectOfType<DialogueManager>().StartDialogue(DialogueCreator());
    }
    public void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Player")
        {
            TriggerDialogue();
        }
    }
}
