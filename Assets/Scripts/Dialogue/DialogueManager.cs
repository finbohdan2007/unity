using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public Text nameText;
    public Text dialogueText;
    public Animator animator;
    public Button button;
    public Queue<string> sentences;
    public Animator chel;

    void Start()
    {
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(DisplayNextSentence);
    }
    void Update()
    {
        if(animator.GetBool("IsOpen") && Input.GetKeyDown(KeyCode.Return))
        {
            DisplayNextSentence();
        }
    }

    public void StartDialogue(Dialogue dialogue)
    {
        sentences = new Queue<string>();
        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue (sentence);
        }
        if(sentences.Count != 0)
        {
            animator.SetBool("IsOpen", true);
        }
        DisplayNextSentence();
        chel.SetBool("isSpeaking", true);
    }
    public void DisplayNextSentence()
    {
        if(sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
        string sentence = sentences.Dequeue();
        if (sentence.Contains("["))
        {
            nameText.text = sentence;
            DisplayNextSentence();
        }
        else
        {
            dialogueText.text = sentence;
            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentence));
        }
    }
    public IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach(char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return new WaitForSeconds(0.1f);
        }
    }
    void EndDialogue()
    {
        dialogueText.text = " ";
        animator.SetBool("IsOpen", false);
        chel.SetBool("isSpeaking", false);
    }
}
