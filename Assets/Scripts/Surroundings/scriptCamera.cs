using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptCamera : MonoBehaviour
{
    [SerializeField] Transform playerPosition;



    void Update()
    {
        Vector3 newCameraPosition = new Vector3(playerPosition.position.x, playerPosition.position.y, -10);
        gameObject.transform.position = newCameraPosition;
    }
}
