using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Menu : MonoBehaviour
{
    [SerializeField] Button launch;
    [SerializeField] Button settings;
    [SerializeField] Button exit;
    [SerializeField] GameObject panel;
    [SerializeField] Dropdown resolutionDropdown;
    [SerializeField] Toggle fullscreenMode;
    Resolution[] resolutions;

    public GameObject optionsScreen;
    public void HidePanel()
    {
        panel.SetActive(false);
    }
    public void ShowPanel()
    {
        panel.SetActive(true);
    }
    public void LaunchGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    void Start()
    {
        if(resolutionDropdown != null)
        {
            resolutions = Screen.resolutions;
            List<string> resolutionString = new List<string>();
            int currentIndex = 0;
            for(int i = 0; i < resolutions.Length; i++)
            {
                resolutionString.Add($"{resolutions[i].width}x{resolutions[i].height}");
                if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height==Screen.currentResolution.height)
                {
                    currentIndex = i;
                }
            }
            resolutionDropdown.AddOptions(resolutionString);
            resolutionDropdown.value = currentIndex;
            resolutionDropdown.RefreshShownValue();
        }
        HidePanel();
    }

    public void SetResolution(int index)
    {
        var res = Screen.resolutions[index];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);
    }

    public void SetFullScreenMode(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void doExitGame()
    {
        Application.Quit();
    }

    void Update()
    {
        
    }

    public void OpenOptions()
    {
        optionsScreen.SetActive(true);
    }
    public void CloseOptions()
    {
        optionsScreen.SetActive(false);
    }
}
