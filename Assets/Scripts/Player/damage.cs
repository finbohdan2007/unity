using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class damage : MonoBehaviour
{
    [SerializeField] float AttackRange = 5f;
    [SerializeField] int AttackDamage = 3;
    [SerializeField] float NextAttackTime = 0;

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, AttackRange);
    }
    void OnTriggerEnter2D(Collider2D entity)
    {
        Debug.Log("Log3");
        if (entity.gameObject.tag == "Player")
        {
            Debug.Log("Log2");
            Damage(AttackDamage, entity.gameObject.GetComponent<Health>());
        }


    }
    void OnTriggerStay2D(Collider2D entity)
    {
        if (entity.gameObject.tag == "Player")
        {
            Debug.Log("Log");
            Damage(AttackDamage, entity.gameObject.GetComponent<Health>());
        }


    }
    void Damage(int damage, Health hp)
    {
        if (NextAttackTime < Time.time)
        {
            hp.TakeDamage(damage);
            NextAttackTime = Time.time;
            NextAttackTime = Time.time + 1f / 2f;
        }
    }
}
