using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public int health = 100;
    public int maxHealth = 100;
    [SerializeField] Slider slider;

    void Start()
    {
        slider.minValue = 0;
        slider.maxValue = maxHealth;
        slider.value = health;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
            AudioManager.Instance.PlaySFX("game over");
        }
        UpdateHPBarValue();
    }
    void Update()
    {
        
    }
    void UpdateHPBarValue()
    {
        slider.value = health;
    }
    void Die()
    {
        SceneManager.LoadScene(0);
    }
    
}
