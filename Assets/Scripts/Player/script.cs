using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script : MonoBehaviour

{
    [SerializeField] Rigidbody2D rb;
    [SerializeField] SpriteRenderer sr;
    [SerializeField] GameObject go;
    [SerializeField] Animator animator;
    [SerializeField] float speed = 5f;
    [SerializeField] float jumpSpeed = 14f;
    [SerializeField] int DashRange = 15;
    bool isDashed = false;
    bool isJumped = false;
    private bool canJump = true;


    void Update()
    {
        if (Physics2D.Linecast(transform.position, go.GetComponent<Transform>().position, 1 << LayerMask.NameToLayer("Ground")))
        {
            canJump = true;
            isDashed = false;
            
            isJumped = false;
        }
        if (Input.GetKey(KeyCode.D) && !isDashed)
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
            animator.SetBool("isWalking", true);
            sr.flipX = false;
            AudioManager.Instance.PlaySFX("run");
        }
        else if (Input.GetKey(KeyCode.A) && !isDashed)
        {
            rb.velocity = new Vector2(-speed, rb.velocity.y);
            animator.SetBool("isWalking", true);
            sr.flipX = true;
            AudioManager.Instance.PlaySFX("run");
        }
        else
        {
            animator.SetBool("isWalking", false);
        }

        if (Input.GetKey(KeyCode.S) && !isDashed)
        {
            animator.SetBool("IsSitting", true);
            AudioManager.Instance.PlaySFX("run");
        }
        else
        {
            animator.SetBool("IsSitting", false);
        }

        if (Input.GetKeyDown(KeyCode.Space) && canJump == true && !isDashed)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
            canJump = false;
            animator.SetBool("isJumping", true);
            AudioManager.Instance.PlaySFX("jump");
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            animator.SetBool("isJumping", false);
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Dash();
            animator.SetBool("isDashing", true);
            AudioManager.Instance.PlaySFX("dash");
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            animator.SetBool("isDashing", false);
        }


    }

    void Dash()
    {
        if(!isDashed)
        {
            isDashed = true;
            if(sr.flipX == true)
            {
                rb.velocity = new Vector2(-0.8f * DashRange, 0);
            } 
            else
            {
                rb.velocity = new Vector2(0.8f * DashRange, 0);
            }
        }
    }   
}